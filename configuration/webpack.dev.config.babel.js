import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const BUILD_DIR = path.resolve(__dirname, '../dist');
const APP_DIR = path.resolve(__dirname, '../src');

const BASE_EXTENSIONS = ['.js'];

export default {
  entry: [
    'react-hot-loader/patch',
    `${APP_DIR}/index.jsx`,
  ],
  devServer: {
    contentBase: BUILD_DIR,
    compress: true,
    port: 8089,
    open: true,
    hot: true,
  },
  output: {
    path: BUILD_DIR,
    filename: 'main_dev.js',
  },
  module: {
    loaders: [
      {
        test: /\.js[x]+/,
        loader: ['react-hot-loader/webpack', 'babel-loader'],
      },
      {
        test: /\.css$/,
        loaders: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[hash:base64:8]',
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: path.resolve(__dirname, './postcss.config.js'),
              },
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: [...BASE_EXTENSIONS, ...['.jsx', '.css', '.json']],
  },
  devtool: 'eval-source-map',
  plugins: [
    new HtmlWebpackPlugin({
      title: 'code_corn_time_app_components',
      template: 'src/index.html',
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
};
