function cssConfig() {
  return {
    plugins: {
      'postcss-icss-values': true,
      'postcss-cssnext': true,
    },
  };
}
module.exports = cssConfig;
