import UglifyJSPlugin from 'uglifyjs-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import webpack from 'webpack';
import path from 'path';
import config from './webpack.dev.config.babel';

const BUILD_DIR = path.resolve(__dirname, '../dist');
const APP_DIR = path.resolve(__dirname, '../src');

const prodConfig = {
  entry: {
    main: `${APP_DIR}/prod.jsx`,
    vendor: [
      'react',
      'react-css-modules',
      'coder_corn_time_app_compontents',
      'react-dom',
      'react-redux',
      'redux',
    ],
  },
  output: {
    path: BUILD_DIR,
    filename: 'main.js',
  },
  devtool: false,
  devServer: {},
  module: {
    loaders: config.module.loaders.map((configuration) => {
      if (configuration.test.test('.css')) {
        const overridenConfiguration = {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  importLoaders: 1,
                  localIdentName: '[hash:base64:8]',
                  minimize: true,
                },
              },
              {
                loader: 'postcss-loader',
                options: {
                  config: {
                    path: path.resolve(__dirname, './postcss.config.js'),
                  },
                },
              },
            ],
          }),
        };
        return overridenConfiguration;
      }
      return configuration;
    }),
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.js',
      minChunks: Infinity,
    }),
    new webpack.optimize.CommonsChunkPlugin({ name: 'meta', chunks: ['vendor'], filename: 'meta.js' }),
    new UglifyJSPlugin({
      parallel: true,
      uglifyOptions: {
        compress: {
          warnings: false,
          dead_code: true,
          drop_console: true,
          drop_debugger: true,
        },
      },
    }),
    new ExtractTextPlugin({
      filename: 'main.css',
      allChunks: true,
      ignoreOrder: true,
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'production'),
    }),
  ],
};
export default Object.assign({}, config, prodConfig);
