import React from 'react';
import ReactDom from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import Main from './main';

window.onload = () => {

  const render = (Component) => {
    ReactDom.render(
      <AppContainer>
        <Component {...{}} />
      </AppContainer>
      ,
      document.getElementById('content'),
    );
  };

  render(Main);

  if (module.hot) {
    module.hot.accept('./main.jsx', () => {
      const NextMain = require('./main.jsx').default;
      render(NextMain);
    });
  }
};
