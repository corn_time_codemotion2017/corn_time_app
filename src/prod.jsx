import React from 'react';
import ReactDom from 'react-dom';
import Main from './main';

window.onload = () => {
  const render = (Component) => {
    ReactDom.hydrate(
      <Component {...window.INITIAL_STATUS} />
      ,
      document.getElementById('content'),
    );
  };

  render(Main);
};
