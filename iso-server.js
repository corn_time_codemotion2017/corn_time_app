import express from 'express';
import compression from 'compression';
import React from 'react';
import {renderToString} from 'react-dom/server';
import Main from './src/main';
import {initialState} from './page/reducer_action';

const PORT = process.env.PORT || 8090;
const options = {
  etag: true,
  index: true,
  extensions: ['html'],
  maxAge: '31536000'
}
const app = express(options);
app.use(compression({level: 9, memLevel: 9}));
app.use(express.static('dist'));
app.get('/', (req, res) => {
  const initStatus = Object.assign({}, initialState);
  const stringContent = renderToString(
    <Main {...initStatus} />
  );
  res.send(`
  <html lang="en">
    <head>
      <meta charset="UTF-8">
      <link rel="stylesheet" type="text/css" href="main.css" />
      <style>
          body {
              background: #1b1b1b;
          }
      </style>
    </head>
    <body>
        <div id="content">${stringContent}</div>
        <script type="text/javascript">
            window.INITIAL_STATUS = ${JSON.stringify(initStatus)}
        </script>
        <script type="text/javascript" src="meta.js"></script>
        <script type="text/javascript" src="vendor.js"></script>
        <script type="text/javascript" src="main.js"></script>
    </body>
  </html>
  `);
});
app.listen(PORT, () => {
  console.log(`running on ${PORT}`);
});
